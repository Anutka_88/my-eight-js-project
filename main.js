const paragraph = document.querySelectorAll("p");
paragraph.forEach((element)=> {
   element.style.backgroundColor = "#ff0000"; 
});

const elem = document.getElementById("optionsList");
console.log(elem);
const parent = elem.parentElement
console.log(parent);
for (let node of elem.childNodes) {
   console.log(node, node.nodeType);
 };

const newElem = document.createElement("p");
newElem.classList.add("testParagraph");
newElem.innerHTML = "This is a paragraph";
elem.appendChild(newElem);

const otherElem = document.querySelector(".main-header");
const insElem = otherElem.getElementsByTagName("*");
console.log(insElem);
for(let i=0; i < insElem.length; i++) {
    insElem[i].className = "nav-item";
};

const lastElem = document.querySelectorAll(".section-title");
lastElem.forEach((i) => {
    i.classList.remove("section-title");
});